public class Bank {
    private int fund = 50000;

    public int getFund(){
        return fund;
    }

    public void setFund(int fund) {
        this.fund = fund;
    }
}
