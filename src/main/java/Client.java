public class Client {
    Bank bank = new Bank();
    public void newClient(){
        double action = Math.random();
        if(action > 0.5){
            new Thread(() -> {
                int deposit = (int)(1000 + Math.random() * 19000);
                bank.setFund(bank.getFund() + deposit);

            }).start();
        }else {
            new Thread(() -> {
                int credit = (int)(500 + Math.random() * 19500);
                bank.setFund(bank.getFund() - credit);
            }).start();
        }
    }
}
